var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    return gulp
        .src('./src/scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/css/'));
});

gulp.task('watcher', function() {
    gulp.watch('./src/scss/**/*.scss', ['sass']);
});

gulp.task('build', ['sass', 'watcher']);
