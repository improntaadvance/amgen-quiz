import React from 'react';
import ReactDOM from 'react-dom';
import App from './views/App';
// import registerServiceWorker from './registerServiceWorker';

function downloadObjectAsJson(exportObj, exportName) {
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href", dataStr);
    downloadAnchorNode.setAttribute("download", exportName + ".json");
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
}

window.onbeforeunload = () => downloadObjectAsJson(localStorage, "risultati");

ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();
