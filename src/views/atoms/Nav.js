import React from 'react';
import arrow from '../../media/images/arrow.svg';
import { Link } from 'react-router-dom';

const Nav = props => {
    let label = <label className="Nav__label">{props.label}</label>;

    return (
        <Link to={props.link} onClick={e => props.check(e)} className={`Nav ${props.position}`}>
            {props.label.length > 0 ? label : null}
            <img className="Nav__image" src={arrow} />
        </Link>
    );
};

Nav.defaultProps = {
    position: 'center',
    label: '',
    link: '/'
};

export default Nav;