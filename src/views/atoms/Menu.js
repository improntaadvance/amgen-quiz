import React from 'react';
import { Link } from 'react-router-dom';
import quizlink from '../../media/images/quiz.svg';
import videolink from '../../media/images/video.svg';

const Options = () => {
    return (
        <div className="Menu">
            <Link className="Menu__link" to="quiz">
                <img className="Menu__image" src={quizlink} alt="" />
            </Link>
            <Link className="Menu__link" to="video">
                <img className="Menu__image" src={videolink} alt="" />
            </Link>
        </div>
    );
};

export default Options;