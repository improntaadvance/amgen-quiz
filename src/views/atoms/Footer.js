import React from 'react';

const Footer = (props) => (
    <footer className="Footer">
        {props.children}
    </footer>
);

export default Footer;

