import React from 'react';

class Choice extends React.Component {
    constructor(props) {
        super(props);
        this.handleSelectChoice = this.handleSelectChoice.bind(this);
    }
    handleSelectChoice(e) {
        e.preventDefault();

        const id = this.props.index;
        this.props.onChoose(id);
    }
    render() {
        return (
            <div className="Choices" onClick={this.handleSelectChoice}>
                <div className="Choice">
                    <input
                        className="Choice__checkbox"
                        type="checkbox"
                        id={this.props.index}
                        checked={this.props.selected}
                    />
                    <label className="Choice__label" htmlFor={this.props.index}>
                        {this.props.children}
                    </label>
                </div>
            </div>
        );
    }
};

export default Choice;