import React from 'react';

const Todrag = props => {
    return (
        <div className="Draggable">
            <div className="Draggable__wrapper">
                <span className="Draggable__sup">{props.item.sup}</span>
                <span className="Draggable__title">{props.item.title}</span>
                <span className="Draggable__sub">{props.item.sub}</span>
            </div>
        </div>
    );
};

export default Todrag;
