import React from 'react';
import { DropTarget } from 'react-dnd';
import { ItemTypes } from '../Constants';

const answerboxTarget = {
    drop(props, monitor, component) {
        props.onDrop(monitor.getItem());
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver()
    };
}

class Droppable extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { connectDropTarget, isOver } = this.props;

        return connectDropTarget(
            <div className={`Droppable ${isOver ? 'over' : ''}`}>
                {this.props.children}
            </div>
        );
    }
}

export default DropTarget(ItemTypes.DRAGGABLE, answerboxTarget, collect)(Droppable);
