import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ItemTypes } from '../Constants';
import { DragSource } from 'react-dnd';
import Todrag from '../atoms/Todrag';

const druggableSource = {
    beginDrag(props) {
        return {
            id: props.id,
            title: props.item.title,
            sup: props.item.sup,
            sub: props.item.sub,
            show: props.item.show
        };
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    };
}

class Draggable extends Component {
    render() {
        const { connectDragPreview, connectDragSource, isDragging, item, empty } = this.props;
        const { title, sup, sub } = item;

        if(empty) {
            return <div className="Draggable" />;
        }

        let draggable = (
            <div style={{ opacity: isDragging ? 0.5 : 1, display: 'inline-block', cursor: 'move' }}>
                <Todrag item={item} />
            </div>
        );

        draggable = connectDragSource(draggable);
        draggable = connectDragPreview(draggable);

        return draggable;
    }
}

Draggable.propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired
};

export default DragSource(ItemTypes.DRAGGABLE, druggableSource, collect)(Draggable);