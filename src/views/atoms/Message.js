import React from 'react';

const Message = props => {
    return(
        <div className="Message">
            <div className={`Message__content ${props.correct ? 'yes' : 'nope'}`}>
                <p>Risposta {props.correct ? 'esatta!' : 'sbagliata.'}</p>
            </div>
        </div>
    );
};

export default Message;