import React from 'react';
import DragLayer from 'react-dnd/lib/DragLayer';
import Todrag from './Todrag';

function collect (monitor) {
    var item = monitor.getItem();

    return {
        item,
        currentOffset: monitor.getSourceClientOffset(),
        isDragging: monitor.isDragging()
    };
}

function getItemStyles (currentOffset) {
    if (!currentOffset) {
        return {
            display: 'none'
        };
    }

    var x = currentOffset.x;
    var y = currentOffset.y;
    var transform = `translate(${x}px, ${y}px)`;

    return {
        pointerEvents: 'none',
        transform: transform,
        WebkitTransform: transform,
        opacity: 0.5,
        display: 'inline-block',
        cursor: 'move'
    };
}

function ItemPreview ({
    isDragging,
    currentOffset,
    item
}) {
    if (!isDragging) {
        return null;
    }

    return (
        <div
            className="item preview"
            style={getItemStyles(currentOffset)}
        >
            <Todrag item={item} />
        </div>
    );
}

export default DragLayer(collect)(ItemPreview);