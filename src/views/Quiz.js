import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Nav from './atoms/Nav';
import Intro from './quiz/Intro';
import Quiz1 from './quiz/Quiz1';
import Quiz2 from './quiz/Quiz2';
import Quiz3 from './quiz/Quiz3';
import Quiz4 from './quiz/Quiz4';
import Quiz5 from './quiz/Quiz5';
import Quiz6 from './quiz/Quiz6';
import Result from './quiz/Result';

const results = {
    quiz1: null,
    quiz2: null,
    quiz3: null,
    quiz4: null,
    quiz5: null,
    quiz6: null
};

class Quiz extends React.Component {
    state = {
        selections: [],
        corrects: 0,
    }

    handleAddPoints = (quiz, result, selection) => {
        results[quiz] = result;

        let corrects = 0;
        for(const prop in results) {
            if (results[prop]) {
                corrects ++;
            }
        };

        const thisQuiz = {
            [quiz]: selection
        };

        const thisQuizSelections = this.state.selections;
        thisQuizSelections.push(thisQuiz);

        this.setState({selections: thisQuizSelections, corrects});
    }

    setSelections = e => {
        console.log(this.state.selections);

        let selectionsString = JSON.stringify(this.state.selections);
        localStorage.setItem(Date.now(), selectionsString);
    }

    render() {
        return (
            <div className="Container relative">
                <Route exact path="/quiz" component={Intro} />
                <Route
                    exact
                    path="/quiz/1"
                    render={props => (
                        <Quiz1
                            handleAddPoints={this.handleAddPoints}
                            {...props}
                        />
                    )}
                />
                <Route
                    exact
                    path="/quiz/2"
                    render={props => (
                        <Quiz2
                            handleAddPoints={this.handleAddPoints}
                            {...props}
                        />
                    )}
                />
                <Route
                    exact
                    path="/quiz/3"
                    render={props => (
                        <Quiz3
                            quiz3={results.quiz3}
                            handleAddPoints={this.handleAddPoints}
                            {...props}
                        />
                    )}
                />
                <Route
                    exact
                    path="/quiz/4"
                    render={props => (
                        <Quiz4
                            handleAddPoints={this.handleAddPoints}
                            {...props}
                        />
                    )}
                />
                <Route
                    exact
                    path="/quiz/5"
                    render={props => (
                        <Quiz5
                            handleAddPoints={this.handleAddPoints}
                            {...props}
                        />
                    )}
                />
                <Route
                    exact
                    path="/quiz/6"
                    render={props => (
                        <Quiz6
                            handleAddPoints={this.handleAddPoints}
                            {...props}
                        />
                    )}
                />
                <Route
                    path="/quiz/result"
                    render={props => (
                        <Result
                            setSelections={this.setSelections}
                            results={this.state.corrects}
                            {...props}
                        />
                    )}
                />
            </div>
        );
    }
}

export default Quiz;