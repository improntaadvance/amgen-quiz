import React from 'react';
import Videomenu from './video/Videomenu';
import Video1 from './video/Video1';
import Video2 from './video/Video2';
import Video3 from './video/Video3';
import Video4 from './video/Video4';
import { Route } from 'react-router-dom';

const Video = () => (
    <div className="Video__wrapper">
        <Route exact path="/video" component={Videomenu} />
        <Route exact path="/video/1" component={Video1} />
        <Route exact path="/video/2" component={Video2} />
        <Route exact path="/video/3" component={Video3} />
        <Route exact path="/video/4" component={Video4} />
    </div>
);

export default Video;