import React, { Component } from 'react';
import Home from './Home.js';
import Quiz from './Quiz.js';
import Quiz1 from './quiz/Quiz1.js';
import Quiz2 from './quiz/Quiz2.js';
import Quiz3 from './quiz/Quiz3.js';
import Quiz4 from './quiz/Quiz4.js';
import Quiz5 from './quiz/Quiz5.js';
import Quiz6 from './quiz/Quiz6.js';
import Result from './quiz/Result.js';
import Video from './Video.js';
import { HashRouter as Router, Route, Link, Switch } from 'react-router-dom';

class App extends React.Component {
    render() {
        return (
            <Router>
                <div className="App Container">
                    <Switch>
                        <Route path="/" exact component={Home} />
                        <Route
                            path="/quiz"
                            render={props => (
                                <Quiz
                                    setSelections={this.setSelections}
                                    {...props}
                                />
                            )}
                        />
                        <Route path="/video" component={Video} />
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
