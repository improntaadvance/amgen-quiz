import React from 'react';
import Menu from './atoms/Menu.js';
import Footer from './atoms/Footer.js';

import logo from '../media/images/logo.svg';

const Home = () => (
    <div className="Container">
        <Menu />
        <Footer>
            <img className="Footer__logo" src={logo} alt="" />
        </Footer>
    </div>
);

export default Home;