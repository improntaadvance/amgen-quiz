import React from 'react';
import Nav from '../atoms/Nav';
import arrow from '../../media/images/arrow.svg';
import pillola1 from '../../media/videos/pillola-1.mp4';
import { Link } from 'react-router-dom';

const Video1 = () => (
    <div className="Video">
        <Link className="Nav top" to={'/video'}>
            <label className="Nav__label">Indietro</label>
            <img className="Nav__image" src={arrow} />
        </Link>
        <video className="video" autoPlay playsInline>
            <source src={pillola1} />
        </video>
    </div>
);

export default Video1;