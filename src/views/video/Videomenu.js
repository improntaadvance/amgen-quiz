import React from 'react';
import arrow from '../../media/images/arrow.svg';
import video1 from '../../media/images/video-1.jpg';
import video2 from '../../media/images/video-2.jpg';
import video3 from '../../media/images/video-3.jpg';
import video4 from '../../media/images/video-4.jpg';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

const Videomenu = () => (
    <div className="Video__menu">
        <Link className="Nav top" to={'/'}>
            <label className="Nav__label">Menù</label>
            <img className="Nav__image" src={arrow} />
        </Link>
        <div className="Video__menu-single">
            <Link className="Video__link" to={'/video/1'}>
                <img src={video1} />
                <h5>RWD E RWE COSA SONO</h5>
            </Link>
        </div>
        <div className="Video__menu-single">
            <Link className="Video__link" to={'/video/2'}>
                <img src={video2} />
                <h5>CHI HA BISOGNO DELLE RWE</h5>
            </Link>
        </div>
        <div className="Video__menu-single">
            <Link className="Video__link" to={'/video/3'}>
                <img src={video3} />
                <h5>ESEMPI DI RWD IN ITALIA</h5>
            </Link>
        </div>
        <div className="Video__menu-single">
            <Link className="Video__link" to={'/video/4'}>
                <img src={video4} />
                <h5>VBH</h5>
            </Link>
        </div>
    </div>
);

export default Videomenu;