import React from 'react';
import Nav from '../atoms/Nav';
import arrow from '../../media/images/arrow.svg';
import pillola4 from '../../media/videos/pillola-4.mp4';
import { Link } from 'react-router-dom';

const Video4 = () => (
    <div className="Video">
        <Link className="Nav top" to={'/video'}>
            <label className="Nav__label">Indietro</label>
            <img className="Nav__image" src={arrow} />
        </Link>
        <video className="video" autoPlay playsInline>
            <source src={pillola4} />
        </video>
    </div>
);

export default Video4;