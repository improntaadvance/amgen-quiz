import React from 'react';
import Draggable from '../atoms/Draggable';
import Droppable from '../atoms/Droppable';
import Footer from '../atoms/Footer';
import Nav from '../atoms/Nav';
import Todrag from '../atoms/Todrag';
import Message from '../atoms/Message';
import { DragDropContext } from 'react-dnd';
// import TouchBackend from 'react-dnd-html5-backend';
import { default as TouchBackend } from 'react-dnd-touch-backend';
import ItemPreview from '../atoms/ItemPreview';

let questions = {
    1: {
        id: 1,
        title: 'Analisi di costo efficacia',
        sup: '',
        sub: '',
        show: true
    },
    2: {
        id: 2,
        title: 'Analisi di impatto sul budget',
        sup: '',
        sub: '',
        show: true
    }
};

const results = {
    1: 2,
    2: 1
};

const selections = {
    1: null,
    2: null
};

class Quiz6 extends React.Component {
    state = {
        result: false,
        dropped: {...selections},
        showMessage: false,
        checked: false
    }

    resetState = e => {
        e.preventDefault();

        this.setState({
            dropped: {...selections},
            result: false
        });
    }

    onDrop = (itemId, area) => {
        let dropped = this.state.dropped;
        dropped[area] = itemId;

        const result = Object.keys(dropped).every(prop => {
            return dropped[prop] === results[prop];
        });

        this.setState({result, dropped});
    }

    check = e => {
        if(!this.state.checked) {
            e.preventDefault();

            this.props.handleAddPoints('quiz6', this.state.result, this.state.dropped);

            this.setState({
                showMessage: true,
                checked: true
            });
        }
    }

    render() {
        return (
            <div className="Container">
                <Nav link="/quiz/result" check={this.check} />
                <div className="Quiz6">
                    <h1 className="text-center">
                        CORRELA IL TIPO DI ANALISI ALLA FRASE CORRISPONDENTE<sup>3,7</sup>
                    </h1>
                    <div className="white-space-x3" />
                    <div className="text-center">
                        {Object.keys(questions).map(questionKey => (
                            <Draggable
                                id={questionKey}
                                key={questionKey}
                                item={questions[questionKey]}
                            />
                        ))}
                    </div>
                    <ItemPreview key="__preview" name="Item" />

                    <div className="white-space-x2" />

                    <div className="text-center">
                        <button className="Reset__button" onClick={this.resetState}>
                            Riposiziona le risposte
                        </button>
                    </div>

                    <div className="white-space-x3" />
                    <div className="white-space-x3" />

                    <div className="Droppable__option">
                        <div className="Droppable__areas">
                            {Object.keys(this.state.dropped).map(area => (
                                <Droppable key={area} onDrop={item => this.onDrop(Number(item.id), area)}>
                                    {this.state.dropped[area] ? <Todrag item={questions[this.state.dropped[area]]} /> : null}
                                </Droppable>
                            ))
                            }
                        </div>
                        <div className="Droppable__texts">
                            <p className="Droppable__text">
                                IL SUO PRINCIPALE OBIETTIVO DOVREBBE CONSISTERE NEL
                                VALUTARE LE CONSEGUENZE FINANZIARIE
                                DELL’INTRODUZIONE DI UNA NUOVA TECNOLOGIA NEL BREVE
                                TERMINE, IN UNO SPECIFICO CONTESTO TERRITORIALE,
                                ALLO SCOPO DI DETERMINARNE LA SOSTENIBILITÀ DA PARTE
                                DEL SISTEMA SANITARIO DI RIFERIMENTO.
                            </p>
                            <p className="Droppable__text">
                                FORNISCE UN ELEMENTO DI VALUTAZIONE PER IL PAYER PER
                                CONSENTIRE UN’ALLOCAZIONE RAZIONALE DELLE RISORSE AL
                                FINE DI MASSIMIZZARE LA PRODUZIONE DI SALUTE.
                            </p>
                        </div>
                    </div>

                    <div className="white-space-x3" />
                </div>
                <Footer>
                    <p>
                        3. Eichler et al Use of Cost-Effectiveness Analysis in
                        Health-Care Resource Allocation Decision-Making: How Are
                        Cost-Effectiveness Thresholds Expected to Emerge? VALUE
                        IN HEALTH Volume 7 • Number 5 • 2004
                        <br />7. Duranti et al, Revisione critica delle BIA
                        condotte in Europa, Quaderni di farmacoeconomia 22 Nov
                        2013
                    </p>
                </Footer>
                {this.state.checked && !this.state.result ? (
                    <div className="Solution Solution__quiz6">
                        <div className="solutions-wrapper">
                            <div>
                                <span>Analisi di impatto sul budget</span>
                            </div>
                            <div>
                                <span>Analisi di costo efficacia</span>
                            </div>
                        </div>
                    </div>
                ) : null}
                {this.state.showMessage ? <Message correct={this.state.result} /> : null}
            </div>
        );
    }
}

export default DragDropContext(TouchBackend({ enableMouseEvents: true }))(Quiz6);