import React from 'react';
import Footer from '../atoms/Footer';
import Choice from '../atoms/Choice';
import Nav from '../atoms/Nav';
import Message from '../atoms/Message';

class Quiz5 extends React.Component {
    state = {
        result: false,
        selected: null,
        showMessage: false,
        checked: false
    }

    handleResult = (index) => {
        let result = (index === 3);
        this.setState({result, selected: index});
    }

    check = e => {
        if(!this.state.checked) {
            e.preventDefault();

            this.props.handleAddPoints('quiz5', this.state.result, this.state.selected);

            this.setState({
                showMessage: true,
                checked: true
            });
        }
    }

    render() {
        return (
            <div className="Container">
                <Nav link="/quiz/6" check={this.check} />
                <div className="Quiz5">
                    <h1 className="text-center">
                QUALI TRA LE SEGUENTI EVIDENZE POSSONO ESSERE INCLUSE
                        <br />ALL’INTERNO DI UN’ANALISI DI COSTO EFFICACIA?<sup>6</sup>
                    </h1>
                    <div className="Quiz5__choices">
                        <Choice
                            index={1}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 1}
                        >
                    STUDI CLINICI RANDOMIZZATI E DATI REAL WORLD
                        </Choice>
                        <Choice
                            index={2}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 2}
                        >
                    REVISIONI SISTEMATICHE E META-ANALISI
                        </Choice>
                        <Choice
                            index={3}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 3}
                        >
                    TUTTE LE PRECEDENTI
                        </Choice>
                        {(this.state.showMessage && !this.state.result) ? (
                            <div className="Solution Solution__checkbox checkbox__quiz5">
                                <span></span>
                            </div>
                        ) : null}
                    </div>
                </div>
                <Footer>
                    <p>
                6. Saramago et al., Deriving Input Parameters for Cost-Effectiveness
                Modeling: Taxonomy of Data Types and Approaches to Their Statistical
                Synthesis VALUE IN HEALTH 15 (2012) 639 – 649
                    </p>
                </Footer>
                {this.state.showMessage ? <Message correct={this.state.result} /> : null}
            </div>
        );
    }
}

export default Quiz5;
