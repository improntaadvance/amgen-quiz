import React from 'react';
import Footer from '../atoms/Footer';
import Nav from '../atoms/Nav';
import balance from '../../media/images/balance.svg';

class Intro extends React.Component {
    check = e => {}

    render() {
        return (
            <div className="Container">
                <Nav link="/quiz/1" check={this.check} />
                <div className="Intro">
                    <img className="Intro__image" src={balance} alt="balance" />
                    <h1 className="Intro__title">HEALTH ECONOMICS TEST</h1>
                    <h4 className="Intro__subtitle">
                    MISURA IL TUO LIVELLO DI CONOSCENZA IN ECONOMIA SANITARIA
                    </h4>
                </div>
                <Footer>
                    <p className="text-center">
                    I dati sono raccolti in forma aggregata e anonima.
                    </p>
                </Footer>
            </div>
        );
    }
}

export default Intro;