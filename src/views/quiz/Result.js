import React from 'react';
import Nav from '../atoms/Nav';
import result0 from '../../media/images/0.svg';
import result1 from '../../media/images/1.svg';
import result2 from '../../media/images/2.svg';
import result3 from '../../media/images/3.svg';
import result4 from '../../media/images/4.svg';
import result5 from '../../media/images/5.svg';
import result6 from '../../media/images/6.svg';

const Result = props => {

    const resultImage = {
        0: result0,
        1: result1,
        2: result2,
        3: result3,
        4: result4,
        5: result5,
        6: result6
    };

    return <div className="Container">
        <Nav position="top" label="menù" link="/" check={props.setSelections} />
        <div className="Result">
            <img src={resultImage[props.results]} className="Result__image" alt="" />
            <h2 className="Result__title">Il tuo risultato è</h2>
            <div className="Result__pointbar">
                <div className={`Result__pointbar__point ${props.results >= 1 && 'active'}`} />
                <div className={`Result__pointbar__point ${props.results >= 2 && 'active'}`} />
                <div className={`Result__pointbar__point ${props.results >= 3 && 'active'}`} />
                <div className={`Result__pointbar__point ${props.results >= 4 && 'active'}`} />
                <div className={`Result__pointbar__point ${props.results >= 5 && 'active'}`} />
                <div className={`Result__pointbar__point ${props.results >= 6 && 'active'}`} />
            </div>
            <p className="Result__label">{props.results}/6</p>
        </div>
    </div>;
};

export default Result;