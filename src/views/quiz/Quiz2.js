import React from 'react';
import Footer from '../atoms/Footer';
import Choice from '../atoms/Choice';
import Nav from '../atoms/Nav';
import Message from '../atoms/Message';

class Quiz2 extends React.Component {
    state = {
        result: false,
        selected: null,
        showMessage: false,
        checked: false
    }

    handleResult = (index) => {
        let result = (index === 1);
        this.setState({result, selected: index});
    }

    check = e => {
        if(!this.state.checked) {
            e.preventDefault();

            this.props.handleAddPoints('quiz2', this.state.result, this.state.selected);

            this.setState({
                showMessage: true,
                checked: true
            });
        }
    }

    render() {
        return (
            <div className="Container">
                <Nav link="/quiz/3" check={this.check} />
                <div className="Quiz2">
                    <h1 className="text-center">
                        Indica la formula generica per calcolare
                        <br />il rapporto incrementale di costo efficacia<sup>1</sup>
                    </h1>
                    <div className="Quiz2__choices">
                        <Choice
                            index={1}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 1}
                        >
                            <div className="flex-row cl-light-grey">
                                <div className="padding-r-x2">
                                    <strong>ICER =</strong>
                                </div>
                                <div>
                                    <span>Costo new - Costo reference</span>
                                    <hr />
                                    <span>Efficacia new - Efficacia reference</span>
                                </div>
                            </div>
                        </Choice>
                        {(this.state.showMessage && !this.state.result) ? (
                            <div className="Solution Solution__checkbox checkbox__quiz2">
                                <span></span>
                            </div>
                        ) : null}
                        <Choice
                            index={2}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 2}
                        >
                            <div className="flex-row cl-light-grey">
                                <div className="padding-r-x2">
                                    <strong>ICER =</strong>
                                </div>
                                <div>
                                    <span>Costo reference - Costo new</span>
                                    <hr />
                                    <span>Efficacia reference - Efficacia new</span>
                                </div>
                            </div>
                        </Choice>
                        <Choice
                            index={3}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 3}
                        >
                            <div className="flex-row cl-light-grey">
                                <div className="padding-r-x2">
                                    <strong>ICER =</strong>
                                </div>
                                <div>
                                    <span>Efficacia reference - Efficacia new</span>
                                    <hr />
                                    <span>Costo reference - Costo new</span>
                                </div>
                            </div>
                        </Choice>
                    </div>
                </div>
                <Footer>
                    <p>
                        1. Cohen et al Interpreting the Results of Cost-Effectiveness Studies Am
                        Coll Cardiol. 2008 December 16; 52(25): 2119–2126
                    </p>
                </Footer>
                {this.state.showMessage ? <Message correct={this.state.result} /> : null}
            </div>
        );
    }
}

export default Quiz2;