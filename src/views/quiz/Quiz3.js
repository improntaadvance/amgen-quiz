import React from 'react';
import Footer from '../atoms/Footer';
import Choice from '../atoms/Choice';
import Nav from '../atoms/Nav';
import Message from '../atoms/Message';

class Quiz3 extends React.Component {
    state = {
        result: false,
        selected: null,
        showMessage: false,
        checked: false
    }

    handleResult = (index) => {
        let result = (index === 3);
        this.setState({result, selected: index});
    }

    check = e => {
        if(!this.state.checked) {
            e.preventDefault();

            this.props.handleAddPoints('quiz3', this.state.result, this.state.selected);

            this.setState({
                showMessage: true,
                checked: true
            });
        }
    }

    render() {
        return (
            <div className="Container">
                <Nav link="/quiz/4" check={this.check} />
                <div className="Quiz3">
                    <h1 className="text-center">
                    ALL’INTERNO DELL’ANALISI DI COSTO-EFFICACIA,
                        <br />QUALE UNITÀ DI MISURA DI EFFICACIA È LA PIÙ USATA
                        <br />E RACCOMANDATA DALLA COMUNITÀ ACCADEMICA
                        <br />E DA MOLTI SISTEMI SANITARI NAZIONALI?<sup>3</sup>
                    </h1>
                    <div className="Quiz3__choices">
                        <Choice
                            index={1}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 1}
                        >
                        HR (Hazard Ratio)
                        </Choice>
                        <Choice
                            index={2}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 2}
                        >
                        LYG (Life Years Gained)
                        </Choice>
                        <Choice
                            index={3}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 3}
                        >
                        QALYs (Quality Adjusted Life Years)
                        </Choice>
                        {(this.state.showMessage && !this.state.result) ? (
                            <div className="Solution Solution__checkbox checkbox__quiz3">
                                <span></span>
                            </div>
                        ) : null}
                    </div>
                </div>
                <Footer>
                    <p>
                    3. Eichler et al Use of Cost-Effectiveness Analysis in Health-Care
                    Resource Allocation Decision-Making: How Are Cost-Effectiveness
                    Thresholds Expected to Emerge? VALUE IN HEALTH Volume 7 • Number 5 •
                    2004
                    </p>
                </Footer>
                {this.state.showMessage ? <Message correct={this.state.result} /> : null}
            </div>
        );
    }
}

export default Quiz3;
