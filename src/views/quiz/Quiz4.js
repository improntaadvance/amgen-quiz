import React from 'react';
import Footer from '../atoms/Footer';
import Choice from '../atoms/Choice';
import Nav from '../atoms/Nav';
import Message from '../atoms/Message';

class Quiz4 extends React.Component {
    state = {
        result: false,
        selected: null,
        showMessage: false,
        checked: false
    }

    handleResult = (index) => {
        let result = (index === 1);
        this.setState({result, selected: index});
    }

    check = e => {
        if(!this.state.checked) {
            e.preventDefault();

            this.props.handleAddPoints('quiz4', this.state.result, this.state.selected);

            this.setState({
                showMessage: true,
                checked: true
            });
        }
    }

    render() {
        return (
            <div className="Container">
                <Nav link="/quiz/5" check={this.check} />
                <div className="Quiz4">
                    <h1 className="text-center">
                    SE L’ESITO DI UN’ANALISI DI COSTO EFFICACIA È ICER,
                        <br />PER VERIFICARE LA SUA ACCETTABILITÀ, DEVE ESSERE COMPARATO
                        <br />AD UN VALORE SOGLIA. QUAL È IL VALORE SOGLIA RACCOMANDATO
                        <br />DALL’ORGANIZZAZIONE MONDIALE DELLA SANITÀ?<sup>4,5</sup>
                    </h1>
                    <div className="Quiz4__choices">
                        <Choice
                            index={1}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 1}
                        >
                    Fino a 3 volte il PIL pro capite nazionale ≈ € 81.000 in Italia
                        </Choice>
                        {(this.state.showMessage && !this.state.result) ? (
                            <div className="Solution Solution__checkbox checkbox__quiz4">
                                <span></span>
                            </div>
                        ) : null}
                        <Choice
                            index={2}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 2}
                        >
                    Fino 4 volte il PIL pro capite nazionale ≈ € 108.000 in Italia
                        </Choice>
                        <Choice
                            index={3}
                            onChoose={index => this.handleResult(index)}
                            selected={this.state.selected === 3}
                        >
                    Pari alla metà del PIL pro capite nazionale ≈ € 14.000 in Italia
                        </Choice>
                    </div>
                </div>
                <Footer>
                    <p>
                            4. http//www.istat.it Report ISTAT 12 Dicembre 2016
                        <br />5. Marseille et al. Thresholds for the cost–effectiveness of interventions: alternative approaches. Bull World Health Organ 2015;93:118–124
                    </p>
                </Footer>
                {this.state.showMessage ? <Message correct={this.state.result} /> : null}
            </div>
        );
    }
}

export default Quiz4;
