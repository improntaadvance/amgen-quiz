import React from 'react';
import Draggable from '../atoms/Draggable';
import Droppable from '../atoms/Droppable';
import Footer from '../atoms/Footer';
import Message from '../atoms/Message';
import Nav from '../atoms/Nav';
import Todrag from '../atoms/Todrag';
import { DragDropContext } from 'react-dnd';
import { default as TouchBackend } from 'react-dnd-touch-backend';
import ItemPreview from '../atoms/ItemPreview';
import grid from '../../media/images/grid.svg';

const questions = {
    1: {
        title: 'ICER',
        sub: '(rapporto incrementale di costo efficacia)'
    },
    2: {
        sup: 'il nuovo intervento è',
        title: 'dominante',
        sub: 'rispetto al comparatore'
    },
    3: {
        sup: 'il nuovo intervento è',
        title: 'dominato',
        sub: 'rispetto al comparatore'
    }
};

const results = {
    1: 3,
    2: 1,
    3: 1,
    4: 2
};

const selections = {
    1: null,
    2: null,
    3: null,
    4: null
};

class Quiz1 extends React.Component {
    state = {
        result: false,
        dropped: { ...selections },
        showMessage: false,
        checked: false
    };

    resetState = e => {
        e.preventDefault();

        this.setState({
            dropped: { ...selections },
            result: false
        });
    };

    onDrop = (itemId, area) => {
        let dropped = this.state.dropped;
        dropped[area] = itemId;

        const result = Object.keys(this.state.dropped).every(prop => {
            return this.state.dropped[prop] === results[prop];
        });

        this.setState({ result, dropped });
    };

    check = e => {
        if (!this.state.checked) {
            e.preventDefault();

            this.props.handleAddPoints('quiz1', this.state.result, this.state.dropped);

            this.setState({
                showMessage: true,
                checked: true
            });
        }
    };

    render() {
        return (
            <div className="Container">
                <Nav link="/quiz/2" check={this.check} />
                <div className="Quiz1">
                    <h1 className="text-center">
                        Colloca all'interno dei quadranti del grafico
                        <br />i possibili esiti di un'analisi di costoefficacia<sup>1,2</sup>
                    </h1>
                    <div className="white-space-x1" />
                    <h5 className="text-center">Per completare i quadranti, puoi usare un esito due volte.</h5>

                    <div className="white-space-x3" />

                    <div className="text-center">
                        {Object.keys(questions).map(questionKey => (
                            <Draggable
                                id={questionKey}
                                key={questionKey}
                                item={questions[questionKey]}
                            />
                        ))}
                    </div>
                    <ItemPreview key="__preview" name="Item" />
                    <div className="white-space-x1" />

                    <div className="text-center">
                        <button
                            className="Reset__button"
                            onClick={this.resetState}
                        >
                            Riposiziona le risposte
                        </button>
                    </div>

                    <div className="white-space-x2" />

                    <div className="Grid__solutions">
                        <img
                            className="Grid__bg"
                            src={grid}
                            alt=""
                        />
                        {Object.keys(this.state.dropped).map(area => (
                            <div key={area} className="Grid__droparea">
                                <Droppable
                                    onDrop={item =>
                                        this.onDrop(Number(item.id), area)}
                                >
                                    {this.state.dropped[area] ? (
                                        <Todrag
                                            item={
                                                questions[
                                                    this.state.dropped[area]
                                                ]
                                            }
                                        />
                                    ) : null}
                                </Droppable>
                            </div>
                        ))}
                    </div>
                </div>
                <Footer>
                    <strong>
                        NB. La costo-efficacia è un’analisi comparativa tra due
                        o più alternative terapeutiche espressa in termini di
                        conseguenze cliniche ed economiche.
                    </strong>
                    <ol>
                        <li>
                            Cohen et al Interpreting the Results of
                            Cost-Effectiveness Studies Am Coll Cardiol. 2008
                            December 16; 52(25): 2119–2126
                        </li>
                        <li>
                            Noyes et al, Evidence from Cost-Effectiveness
                            Research, NeuroRx: The Journal of the American
                            Society for Experimental NeuroTherapeutics, Vol. 1,
                            348–355, July 2004
                        </li>
                    </ol>
                </Footer>
                {this.state.checked && !this.state.result ? (
                    <div className="Solution Solution__quiz1">
                        <div className="solutions-wrapper">
                            <div>
                                <span className="sup">Il nuovo intervento è</span>
                                <span className="title">Dominato</span>
                                <span className="sub">Rispetto al compratore</span>
                            </div>
                            <div>
                                <span className="title">ICER</span>
                                <span className="sub">(Rapporto incrementale di costo efficacia)</span>
                            </div>
                            <div>
                                <span className="title">ICER</span>
                                <span className="sub">(Rapporto incrementale di costo efficacia)</span>
                            </div>
                            <div>
                                <span className="sup">Il nuovo intervento è</span>
                                <span className="title">Dominante</span>
                                <span className="sub">Rispetto al compratore</span>
                            </div>
                        </div>
                    </div>
                ) : null}
                {this.state.showMessage ? (
                    <Message correct={this.state.result} />
                ) : null}
            </div>
        );
    }
}

export default DragDropContext(TouchBackend({ enableMouseEvents: true }))(Quiz1);
